﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.States
{
    public class BeginState : IStateBase
    {
        private StateManager manager;   // manager game class type

        /// <summary>
        /// Create the state
        /// </summary>
        /// <returns></returns>
        public BeginState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene00
            if (Application.loadedLevelName != "Scene00")
            {
                Application.LoadLevel("Scene00");
            }
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <returns></returns>
        public void ShowIt()
        {
            // draw the texture store in beginStateSplash variable that is the size of the screen
            GUI.DrawTexture(new Rect(Screen.width/2 - 256 / 2, Screen.height/2 - 128 /2, 256, 128), manager.gameDataRef.beginStateSplash, ScaleMode.StretchToFill);

            GUI.Box(new Rect(Screen.width / 2 - 160 / 2, Screen.height / 2, 160, 120), "Begin State");

            // if button pressed or any keys
            if (GUI.Button(new Rect(10, 10, 250, 60), "Press Here or S Key to Continue") || Input.GetKeyUp(KeyCode.S) )
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }
            Debug.Log("In BeginState");
        }
    }
}